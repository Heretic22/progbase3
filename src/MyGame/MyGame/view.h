#pragma once
#include <SFML/Graphics.hpp>
using namespace sf;

sf::View view;

void setPlayerCoordinateForView(float x, float y) {
	float tempX = x; float tempY = y;

	if (x < 688) tempX = 688;
	if (y < 384) tempY = 384;
	if (y > 1376) tempY = 1376;

	view.setCenter(tempX, tempY);
}