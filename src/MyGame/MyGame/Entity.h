#pragma once
#include <SFML/Graphics.hpp>
#include <iostream> 
#include "view.h"
#include "level.h"
#include <vector>
#include <list>

using namespace sf;



class Entity {
public:
	std::vector<Object> obj;
	float dx, dy, x, y, speed, moveTimer;
	int w, h, health;
	bool life, isMove, onGround;
	Texture texture;
	Sprite sprite;
	String name;
	Entity(Image &image, String Name, float X, float Y, int W, int H) {
		x = X; y = Y; w = W; h = H; name = Name; moveTimer = 0;
		speed = 0; health = 100; dx = 0; dy = 0;
		life = true; onGround = false; isMove = false;
		texture.loadFromImage(image);
		sprite.setTexture(texture);
		sprite.setOrigin(w / 2, h / 2);
	}

	FloatRect getRect() {
		return FloatRect(x, y, w, h);
	}
	virtual void update(float time, float CurrentFrame) = 0;
};

class Player :public Entity {
public:
	enum { left, right, up, down, jump, stay } state;
	int playerScore, view, cpx, cpy;
	bool isShoot, Bullet;

	Player(Image &image, String Name, Level &lev, float X, float Y, int W, int H) :Entity(image, Name, X, Y, W, H) {
		playerScore = 0; state = stay; obj = lev.GetAllObjects(); view = 1; Bullet = true;
		cpx = X; cpy = Y;
		if (name == "Player1") {
			sprite.setTextureRect(IntRect(230, 0, w, h));
		}
	}
	void update(float time, float CurrentFrame)
	{
		control();
		switch (state)
		{
		case right: {
			sprite.setScale(1, 1);
			isMove = true;
			dx = speed;
			if (!onGround) sprite.setTextureRect(IntRect(270, 0, 35, 40));
			else sprite.setTextureRect(IntRect(40 * int(CurrentFrame), 0, 35, 40));
			break;
		}

		case left: {
			sprite.setScale(-1, 1);
			isMove = true;
			dx = -speed;
			if (!onGround) sprite.setTextureRect(IntRect(270, 0, 35, 40));
			else sprite.setTextureRect(IntRect(40 * int(CurrentFrame), 0, 35, 40));
			break;
		}
		case jump: {
			sprite.setTextureRect(IntRect(270, 0, 35, 40));
			break;
		}
		case stay: {
			dx = 0;
			isMove = false;
			sprite.setTextureRect(IntRect(230, 0, w, h));
			break;
		}
		}
		x += dx*time;
		checkCollisionWithMap(dx, 0);
		y += dy*time;
		checkCollisionWithMap(0, dy);
		sprite.setPosition(x + w / 2, y + h / 2);
		if (health <= 0) { life = false; }
		if (!isMove) { speed = 0; }
		setPlayerCoordinateForView(x, y);
		if (life) { setPlayerCoordinateForView(x, y); }
		dy = dy + 0.0015*time;
	}

	float getplayercoordinateX() {
		return x;
	}
	float getplayercoordinateY() {
		return y;
	}

	void checkCollisionWithMap(float Dx, float Dy)
	{
		for (int i = 0; i<obj.size(); i++)
			if (getRect().intersects(obj[i].rect))
			{
				if (obj[i].name == "solid" || obj[i].name == "plat")
				{
					if (Dy>0) { y = obj[i].rect.top - h;  dy = 0; onGround = true; }
					if (Dy<0) { y = obj[i].rect.top + obj[i].rect.height;   dy = 0; }
					if (Dx>0) { x = obj[i].rect.left - w; }
					if (Dx<0) { x = obj[i].rect.left + obj[i].rect.width; }
				}
				if (obj[i].name == "cp") {
					cpx = obj[i].rect.left;
					cpy = obj[i].rect.top;
				}
				if (obj[i].name == "death") {
					x = cpx;
					y = cpy;
				}
			}
	}

	void control() {
		if (Keyboard::isKeyPressed) {
			if (Keyboard::isKeyPressed(Keyboard::Left)) {
				state = left; speed = 0.15; view = 0;
			}
			if (Keyboard::isKeyPressed(Keyboard::Right)) {
				state = right; speed = 0.15; view = 1;
			}

			if ((Keyboard::isKeyPressed(Keyboard::Up)) && (onGround)) {
				state = jump; dy = -0.45; onGround = false;
			}
			if (Keyboard::isKeyPressed(Keyboard::Space)) {
				isShoot = true;
			}
			if (!(Keyboard::isKeyPressed(Keyboard::Left)) && !(Keyboard::isKeyPressed(Keyboard::Right)) && !(Keyboard::isKeyPressed(Keyboard::Up))) {
				state = stay; speed = 0;
			}
			if (!Keyboard::isKeyPressed(Keyboard::Space)) {
				Bullet = true; isShoot = false;
			}
		}
	}
};


class Enemy :public Entity {
public:
	enum {left, right} state;
	Enemy(Image &image, String Name, Level &lvl, float X, float Y, int W, int H) :Entity(image, Name, X, Y, W, H) {
		obj = lvl.GetAllObjects();
		if (name == "EasyEnemy") {
			state = right;
			sprite.setTextureRect(IntRect(50, 110, w, h));
			sprite.setScale(-1, 1);
			dx = 0.1;
		}
		if (name == "miniBoss") {
			health = 5;
			state = right;
			sprite.setTextureRect(IntRect(100, 200, w, h));
			sprite.setScale(-1, 1);
			dx = 0.2;
		}
		if (name == "boss") {
			isMove = true;
			health = 100 ;
			sprite.setTextureRect(IntRect(0, 0, w, h));
			dx = 0.15;
		}
	}

	void checkCollisionWithMap(float Dx, float Dy)
	{
		for (int i = 0; i<obj.size(); i++)
			if (getRect().intersects(obj[i].rect))
			{
				if (obj[i].name == "solid") {
					if (Dy>0) { y = obj[i].rect.top - h;  dy = 0; onGround = true; }
					if (Dy<0) { y = obj[i].rect.top + obj[i].rect.height;   dy = 0; }
					if (Dx>0) { x = obj[i].rect.left - w; 
						if (name == "boss") isMove = false;
					}
					if (Dx<0) { x = obj[i].rect.left + obj[i].rect.width;
						if (name == "boss") isMove = false;
					}
				}
			}
	}

	void update(float time, float CurrentFrame)
	{
		if (name == "EasyEnemy") {
			moveTimer += time; if (moveTimer > 3000) {
				dx *= -1;
				moveTimer = 0;
				if (state == left) {
					sprite.setScale(-1, 1);
					state = right;
				}
				else {
					sprite.setScale(1, 1);
					state = left;
				}
			}
		}
		if (name == "miniBoss") {
			moveTimer += time; if (moveTimer > 2500) {
				dx *= -1;
				moveTimer = 0;
				if (state == left) {
					sprite.setScale(-1, 1);
					state = right;
				}
				else {
					sprite.setScale(1, 1);
					state = left;
				}
			}
		}
		if (name == "boss") {
			if (!isMove) {
				moveTimer += time; if (moveTimer > 2000) {
					dx *= -1;
					moveTimer = 0;
					isMove = true;
				}
			}
		}
			x += dx*time;
			checkCollisionWithMap(dx, 0);
			y += dy*time;
			checkCollisionWithMap(0, dy);
			sprite.setPosition(x + w / 2, y + h / 2);
			if (health <= 0) { life = false; }
			dy = dy + 0.0015*time;
	}
};
class Bullet :public Entity {
public:
	int direction;
	int view;

	Bullet(Image &image, String Name, Level &lvl, float X, float Y, int W, int H, int dir, int v) :Entity(image, Name, X, Y, W, H) {
		obj = lvl.GetAllObjects();
		x = X;
		y = Y;
		view = v;
		sprite.setTextureRect(IntRect(400, 10, w, h));
		direction = dir;
		speed = 0.8;
		w = h = 16;
		life = true;
	}


	void update(float time, float CurrentFrame)
	{
		switch (direction)
		{
		case 0: dx = -0.8; dy = 0;   break;
		case 1: dx = 0.8; dy = 0;   break;
		case 4: {
			if (view == 0) {
				dx = -0.8;
				dy = 0;
			}
			if (view == 1) {
				dx = 0.8;
				dy = 0;
			}
		}
		case 5: {
			if (view == 0) {
				dx = -0.8;
				dy = 0;
			}
			if (view == 1) {
				dx = 0.8;
				dy = 0;
			}
		}
		}

		x += dx*time;
		y += dy*time;

		if (x <= 0) { x = 1; life = false; }
		if (y <= 0) { y = 1; life = false; }

		for (int i = 0; i < obj.size(); i++) {
			if (getRect().intersects(obj[i].rect))
			{
				if (obj[i].name == "solid" || obj[i].name == "plat") {
					life = false;
				}
			}
		}
		sprite.setPosition(x + w / 2, y + h / 2);
		moveTimer += time;
		if (moveTimer>1500) {
			life = false;
			moveTimer = 0;
		}
	}
};
