#include <SFML/Graphics.hpp>
#include <iostream> 
#include "Entity.h"

using namespace sf;

void menu(RenderWindow & window) {
	Texture menuTexture1, menuTexture2, menuBackground;
	menuTexture1.loadFromFile("images/111.png");
	menuTexture2.loadFromFile("images/333.png");
	menuBackground.loadFromFile("images/4.png");
	Sprite menu1(menuTexture1), menu3(menuTexture2), menuBg(menuBackground);
	bool isMenu = 1;
	int menuNum = 0;
	menu1.setPosition(100, 30);
	menu3.setPosition(100, 90);
	menuBg.setPosition(345, 0);

	while (isMenu)
	{
		menu1.setColor(Color::White);
		menu3.setColor(Color::White);
		menuNum = 0;
		window.clear(Color(0, 0, 0));
		if (IntRect(100, 30, 300, 50).contains(Mouse::getPosition(window))) { menu1.setColor(Color::Blue); menuNum = 1; }
		if (IntRect(100, 90, 300, 50).contains(Mouse::getPosition(window))) { menu3.setColor(Color::Blue); menuNum = 2; }
		if (Mouse::isButtonPressed(Mouse::Left))
		{
			if (menuNum == 1) isMenu = false;
			if (menuNum == 2) { window.close(); isMenu = false; }
		}
		window.draw(menuBg);
		window.draw(menu1);
		window.draw(menu3);
		window.display();
	}
}

void end(RenderWindow & window) {
	
	Clock clock;

	Font font; 
	font.loadFromFile("CyrilicOld.ttf");
	Text text("Completed", font, 100);
	text.setColor(Color::Red);
	//text.setStyle(sf::Text::Bold | sf::Text::Underlined);
	

	text.setPosition(view.getCenter().x, view.getCenter().y);
	float Timer = 0;

	while (window.isOpen()) {
	
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 800;
		window.draw(text);
		window.display();
		Timer += time; if (Timer > 3000) window.close();
	
	}
	
}


int main()
{
	RenderWindow window(sf::VideoMode(1376, 768), "");
	menu(window);

	view.reset(sf::FloatRect(0, 0, 1376, 768));

	std::list<Entity*>  entities;
	std::list<Entity*>::iterator it;
	std::list<Entity*>::iterator it2;

	Level lvl;
	lvl.LoadFromFile("map1 � �����.tmx");

	Image easyEnemyImage;
	easyEnemyImage.loadFromFile("Images/enemy.png");

	std::vector<Object> e = lvl.GetObjects("EasyEnemy");
	for (int i = 0; i < e.size(); i++)
		entities.push_back(new Enemy(easyEnemyImage, "EasyEnemy", lvl, e[i].rect.left, e[i].rect.top, 40, 40));
	e = lvl.GetObjects("miniBoss");
	for (int i = 0; i < e.size(); i++)
		entities.push_back(new Enemy(easyEnemyImage, "miniBoss", lvl, e[i].rect.left, e[i].rect.top, 40, 40));
	Object boss = lvl.GetObject("boss");



	Clock clock;
	float CurrentFrame = 0;
	

	Image heroImage;
	heroImage.loadFromFile("Images/contra.png");
	heroImage.createMaskFromColor(Color(255, 255, 255));
	Object player = lvl.GetObject("player");

	Player p(heroImage, "Player1", lvl, player.rect.left, player.rect.top, 30, 40);

	Image bossImage;
	bossImage.loadFromFile("Images/boss.png");
	bossImage.createMaskFromColor(Color(255, 255, 255));
	
	

	entities.push_back(new Enemy(bossImage, "boss", lvl, boss.rect.left, boss.rect.top, 200, 170));

	while (window.isOpen())
	{
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 800;
		

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		if (p.isShoot == true && p.Bullet == true) { 
			p.Bullet = false;
			p.isShoot = false;
			entities.push_back(new Bullet (heroImage, "Bullet", lvl, p.x, p.y+4, 16, 16, p.state, p.view));
		}
		for (it = entities.begin(); it != entities.end();)
		{
			Entity *b = *it;
			b->update(time, 0);
			if (b->life == false) {
				if (b->name == "boss") {
					end(window);
				}
				it = entities.erase(it); 
				delete b; 
			}
			else it++;
		}

		for (it = entities.begin(); it != entities.end(); it++) {
			if ((((*it)->name == "EasyEnemy") || ((*it)->name == "miniBoss") || ((*it)->name == "boss")) && ((*it)->getRect().intersects(p.getRect()))) {
				p.x = p.cpx; p.y = p.cpy;
				//p.life = false;
			}
			for (it2 = entities.begin(); it2 != entities.end(); it2++)
			{
				if ((*it)->getRect() != (*it2)->getRect())
					if (((*it)->getRect().intersects((*it2)->getRect())) && ((*it)->name == "Bullet") && ((*it2)->name == "EasyEnemy"))
					{
						(*it)->life = false;
						(*it2)->life = false;
					}
					if (((*it)->getRect().intersects((*it2)->getRect())) && ((*it)->name == "Bullet") && ((*it2)->name == "miniBoss"))
					{
						(*it)->life = false;
						(*it2)->health -= 1;
					}
					if (((*it)->getRect().intersects((*it2)->getRect())) && ((*it)->name == "Bullet") && ((*it2)->name == "boss"))
					{
						(*it)->life = false;
						(*it2)->health -= 1;
					}
			}
		}

		if(p.isMove == true) CurrentFrame += 0.005*time;
		if (CurrentFrame > 5) CurrentFrame -= 5;
		p.update(time, CurrentFrame);
		window.setView(view);

		window.clear(Color(77, 83, 140));
		lvl.Draw(window);


		for (it = entities.begin(); it != entities.end(); it++) {
			window.draw((*it)->sprite);
		}
		window.draw(p.sprite);
		window.display();
	}

	return 0;
}